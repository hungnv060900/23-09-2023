package com.devcamp.j2345.devcampprovince.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.j2345.devcampprovince.models.District;

public interface IDistrictRepository extends JpaRepository<District, Integer>{
	
}
