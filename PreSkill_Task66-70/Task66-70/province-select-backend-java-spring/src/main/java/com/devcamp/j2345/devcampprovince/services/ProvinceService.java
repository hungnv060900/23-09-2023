package com.devcamp.j2345.devcampprovince.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.devcamp.j2345.devcampprovince.models.Province;
import com.devcamp.j2345.devcampprovince.repositories.IProvinceRepository;

@Service
public class ProvinceService {
	@Autowired
	IProvinceRepository pIProvinceRepository;

	// Hàm service lấy tất cả các Thành Phố
	public ArrayList<Province> getAllProvinces() {
		ArrayList<Province> listOfProvinces = new ArrayList<>();
		pIProvinceRepository.findAll().forEach(listOfProvinces::add);
		return listOfProvinces;
	}

	// Hàm service lấy tất cả các Thành Phố có sử dụng chức năng phân trang
	public List<Province> getAllProvincesByServiceUsingPagination(String page, String size) {
		Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
		List<Province> listProvinceWithPagination = new ArrayList<>();
		pIProvinceRepository.findAll(pageWithFiveElements).forEach(listProvinceWithPagination::add);
		return listProvinceWithPagination;
	}
}
