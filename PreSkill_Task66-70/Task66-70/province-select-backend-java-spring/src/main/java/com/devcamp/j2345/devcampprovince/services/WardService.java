package com.devcamp.j2345.devcampprovince.services;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.j2345.devcampprovince.models.District;
import com.devcamp.j2345.devcampprovince.models.Ward;
import com.devcamp.j2345.devcampprovince.repositories.IDistrictRepository;

@Service
public class WardService {
	@Autowired
	IDistrictRepository pIDistrictRepository;
	
	public Set<Ward> getWardByDistrictId(Integer id) {
		District districtEntity = pIDistrictRepository.getReferenceById(id);
		if(districtEntity != null) {
			return districtEntity.getWards();
		} else {
			return null;
		}
	}
}
