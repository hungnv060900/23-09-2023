package com.devcamp.j2345.devcampprovince.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.j2345.devcampprovince.models.Province;
import com.devcamp.j2345.devcampprovince.services.ProvinceService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProvinceController {
	@Autowired
	ProvinceService pProvinceService;
	
	// API lấy tất cả các Thành Phố (có dùng Service)
	@GetMapping("/provinces")
	public ResponseEntity<List<Province>> getAllProvincesByService() {
		try {
			return new ResponseEntity<>(pProvinceService.getAllProvinces(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	// API lấy tất cả các Thành Phố có sử dụng chức năng phân trang (có dùng Service)
	@GetMapping("/provinces-pagination")
	public ResponseEntity<List<Province>> getAllProvincesByServiceUsingPagination(
			@RequestParam(value = "page", defaultValue = "1") String page,
			@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			return new ResponseEntity<>(pProvinceService.getAllProvincesByServiceUsingPagination(page, size), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
