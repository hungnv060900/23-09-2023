package com.devcamp.j2345.devcampprovince.controllers;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.j2345.devcampprovince.models.Ward;
import com.devcamp.j2345.devcampprovince.services.WardService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class WardController {
	@Autowired
	WardService wardService;
	
	// API lấy các Phường theo mã Quận (dùng Service)
	@GetMapping("/wards/{districtId}")
	public ResponseEntity<Set<Ward>> getWardsByDistrictId(@PathVariable(name = "districtId") Integer id) {
		try {
			Set<Ward> wards = wardService.getWardByDistrictId(id);
			if(wards.size() != 0) {
				return new ResponseEntity<Set<Ward>>(wards, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); 
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}
}
