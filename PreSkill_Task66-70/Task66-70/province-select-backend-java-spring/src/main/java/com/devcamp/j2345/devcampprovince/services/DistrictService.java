package com.devcamp.j2345.devcampprovince.services;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.devcamp.j2345.devcampprovince.models.District;
import com.devcamp.j2345.devcampprovince.models.Province;
import com.devcamp.j2345.devcampprovince.repositories.IProvinceRepository;

@Service
public class DistrictService {
	@Autowired
	IProvinceRepository pIProvinceRepository;
	
	// Hàm Service lấy các Quận thuộc mã Thành Phố
	public Set<District> getDistrictsByProvinceCode(String code) {
		Province provinceEntity = pIProvinceRepository.findByCode(code);
		if(provinceEntity != null) {
			return provinceEntity.getDistricts();
		} else {
			return null;
		}
	}
}
