package com.devcamp.j2345.devcampprovince;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcampProvinceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcampProvinceApplication.class, args);
	}

}
