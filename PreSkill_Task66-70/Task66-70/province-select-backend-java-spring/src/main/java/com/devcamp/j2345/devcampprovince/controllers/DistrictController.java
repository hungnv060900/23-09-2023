package com.devcamp.j2345.devcampprovince.controllers;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.j2345.devcampprovince.models.District;
import com.devcamp.j2345.devcampprovince.services.DistrictService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DistrictController {
	@Autowired
	DistrictService districtService;

	// API lấy tất cả các Quận theo mã Thành Phố (dùng Service)
	@GetMapping("/districts/{provinceCode}")
	public ResponseEntity<Set<District>> getDistrictsByProvinceCode(@PathVariable(name = "provinceCode") String code) {
		try {
			Set<District> districts = districtService.getDistrictsByProvinceCode(code);
			if(districts.size() != 0) {
				return new ResponseEntity<Set<District>>(districts, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
