package com.devcamp.j2345.devcampprovince.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.j2345.devcampprovince.models.Province;

public interface IProvinceRepository extends JpaRepository<Province, Integer>{
	Province findByCode(String code);
}
