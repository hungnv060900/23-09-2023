"use strict";

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// Các thẻ HTML Select Element trên giao diện
var gProvinceSelectElement = $("#province-select");
var gDistrictSelectElement = $("#district-select");
var gWardSelectElement = $("#ward-select");
var gBASE_URL_API = "http://localhost:8080/province/";
var gPROVINCE_SUFFIX = "provinces/";
var gDISTRICT_SUFFIX = "districts/";
var gWARD_SUFFIX = "wards/";

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  // thực hiện tải trang
  onPageLoading();

  // call hàm addContentOfSearchInputOnSelect để customize nội dung của thẻ Search in Select elements
  addContentOfSearchInputOnSelect();

  // Hàm sự kiện khi thẻ select Province thay đổi giá trị (callback function)
  gProvinceSelectElement.on("change", function () {
    gDistrictSelectElement.html("");
    gWardSelectElement.html("");
    var vProvinceCode = $(this).val();
    // call getDistrictsByProvinceCode method
    callAPIToGetDistrictsByProvinceCode(vProvinceCode);
  });

  // Hàm sự kiện khi thẻ select District thay đổi giá trị (callback function)
  gDistrictSelectElement.on("change", function () {
    gWardSelectElement.html("");
    var vDistrictId = $(this).val();
    // call getWardsByDistrictId method
    callAPIToGetWardsByDistrictId(vDistrictId);
  });
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
  // gọi hàm invoke API để tải lên thẻ select Province tất cả các Thành Phố
  callAPIToGetAllProvinces();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
// hàm gọi API để lấy danh sách các Province từ server
function callAPIToGetAllProvinces() {
  $.ajax({
    url: gBASE_URL_API + gPROVINCE_SUFFIX,
    method: "GET",
    success: function (pProvinceObjects) {
      loadDataToProvinceSelect(pProvinceObjects);
    },
    error: function (error) {
      console.error(error);
    },
    complete: function (ajaxContext) {
      // Xử lý sau khi yêu cầu AJAX hoàn tất, không quan trọng thành công hay thất bại
      console.log("Hoàn tất yêu cầu AJAX");
      console.log("HTTP status:", ajaxContext.status); // Lấy mã trạng thái HTTP (ví dụ: 200 OK, 404 Not Found, 500 Internal Server Error)
    },
  });
}

// hàm tải lên danh sách các Province trên thẻ select Province
function loadDataToProvinceSelect(pProvinceList) {
    for (let i = 0; i < pProvinceList.length; i++) {
      var bProvinceOption = $("<option/>");
      bProvinceOption.prop("value", pProvinceList[i].code);
      bProvinceOption.prop("text", pProvinceList[i].name);
      gProvinceSelectElement.append(bProvinceOption);
    }
  }
  
  // hàm gọi API để lấy danh sách các District theo Province Code từ server
  function callAPIToGetDistrictsByProvinceCode(pProvinceCode) {
    $.ajax({
      url: gBASE_URL_API + gDISTRICT_SUFFIX + pProvinceCode,
      method: "GET",
      success: function (resDistricts) {
        loadDataToDistrictSelect(resDistricts);
      },
      error: function (error) {
        console.error(error);
      },
      complete: function(ajaxContext) {
        // Xử lý sau khi yêu cầu AJAX hoàn tất, không quan trọng thành công hay thất bại
        console.log('Hoàn tất yêu cầu AJAX');
        console.log('HTTP status:', ajaxContext.status); // Lấy mã trạng thái HTTP (ví dụ: 200 OK, 404 Not Found, 500 Internal Server Error)
      }
    });
  }
  
  // hàm load data các District vào thẻ select District
  function loadDataToDistrictSelect(pDistrictObjects) {
    if (pDistrictObjects.length > 0) {
      gDistrictSelectElement.prop("disabled", false); // enable thẻ Select nếu có dữ liệu
      for (let i = 0; i < pDistrictObjects.length; i++) {
        var bDistrictOption = $("<option/>");
        bDistrictOption.prop("value", pDistrictObjects[i].id);
        bDistrictOption.prop("text", pDistrictObjects[i].name);
        gDistrictSelectElement.append(bDistrictOption);
      }
    } else {
      inactiveDistrictSelectElements();
      inactiveWardSelectElements();
    }
  }
  
  // hàm gọi API để lấy danh sách các Ward theo District Id từ server
  function callAPIToGetWardsByDistrictId(pDistrictId) {
    $.ajax({
      url: gBASE_URL_API + gWARD_SUFFIX + pDistrictId,
      method: "GET",
      success: function (responseWardObjects) {
        loadDataIntoWardSelect(responseWardObjects);
      },
      error: function (error) {
        console.error(error);
      },
      complete: function(ajaxContext) {
        // Xử lý sau khi yêu cầu AJAX hoàn tất, không quan trọng thành công hay thất bại
        console.log('Hoàn tất yêu cầu AJAX');
        console.log('HTTP status:', ajaxContext.status); // Lấy mã trạng thái HTTP (ví dụ: 200 OK, 404 Not Found, 500 Internal Server Error)
      }
    });
  }
  
  // hàm load dữ liệu các Ward vào thẻ select Ward
  function loadDataIntoWardSelect(pWardObjects) {
    if (pWardObjects.length > 0) {
      gWardSelectElement.prop("disabled", false);
      for (let i = 0; i < pWardObjects.length; i++) {
        var bWardOption = $("<option/>");
        bWardOption.prop("value", pWardObjects[i].id);
        bWardOption.prop("text", pWardObjects[i].name);
        gWardSelectElement.append(bWardOption);
      }
    } else {
      inactiveWardSelectElements();
    }
  }
  
  // hàm inactive các option trong thẻ District
  function inactiveDistrictSelectElements() {
    // clear nội dung các option có trong thẻ select District khi chọn lại 1 Thành Phố
    gDistrictSelectElement.html("");
    // khóa thẻ select khi chọn lại 1 Thành phố
    gDistrictSelectElement.prop("disabled", true);
  }
  
  // hàm inactive các option trong thẻ Ward
  function inactiveWardSelectElements() {
    // clear nội dung các option có trong thẻ select Ward khi chọn lại 1 Thành Phố
    gWardSelectElement.html("");
    // khóa thẻ select khi chọn lại 1 Thành phố
    gWardSelectElement.prop("disabled", true);
  }

// hàm customize nội dung trong thẻ Search của mỗi thẻ Select
function addContentOfSearchInputOnSelect() {
  // customize for Province select
  gProvinceSelectElement.one("select2:open", function (e) {
    $("input.select2-search__field").prop(
      "placeholder", "Nhập tên một Tỉnh/Thành Phố..."
    );
  });

  // customize for District select
  gDistrictSelectElement.one("select2:open", function (e) {
    $("input.select2-search__field").prop(
      "placeholder", "Nhập tên một Quận/Huyện..."
    );
  });

  // customize for Ward select
  gWardSelectElement.one("select2:open", function (e) {
    $("input.select2-search__field").prop(
      "placeholder", "Nhập tên một Phường/Xã..."
    );
  });
}
